MONGO_ID = $(shell docker-compose ps --filter name=mongo -q)
MONGO_IP = $(shell docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(MONGO_ID))

run: _env
	docker-compose up -d
	python3 -m bot

_env:
	printf "MONGO_URL=\"mongodb://$(MONGO_IP):27017/\"" > .env
