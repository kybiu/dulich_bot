import os
from typing import Optional

from dotenv import load_dotenv
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.database import Database


def get_reply(input: str) -> Optional[str]:
    return None if input == "bye" else "Ok"


if __name__ == "__main__":
    load_dotenv()
    mongo_url = os.getenv("MONGO_URL")
    print(mongo_url)
    client = MongoClient(mongo_url)
    db: Database = client.get_database("test")
    collection: Collection = db.get_collection('hihi')
    collection.insert_one({"haha": "hu"})
    while True:
        user_message = input("- ")
        reply = get_reply(user_message)
        if reply:
            print(reply)
        else:
            break
    print("Bye")
